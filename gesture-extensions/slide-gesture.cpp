/*
Copyright: 2021 wangweinan <wangweinan@kylinos.cn>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
*/

#include "slide-gesture.h"

#include <QApplication>
#include <QDebug>
#include <QWidget>
#include <QScroller>
#include <QSettings>
#include <QMouseEvent>
#include <QAbstractItemView>
#include <QTimer>
#include <QScrollBar>

#define LONG_PRESS_TIME 300
#define PROPERTY_NOT_USE_GESTURE "notUseSlideGesture"
#define PROPERTY_SLIDE_NO_SCROLLBAR "slideNoScrollBar"

//#define DEBUG_MODE

namespace UkuiGestureExtension {

SlideGesture::SlideGesture(QObject *parent) : QObject(parent)
{
    // we handle the fake mouse event, not real touch event
    qApp->setAttribute(Qt::AA_SynthesizeTouchForUnhandledMouseEvents, false);

    m_touch_slide_timer = new QTimer(this);
    connect(m_touch_slide_timer, &QTimer::timeout, this, [this](){
       m_touch_slide_timer->stop();
    });

    //! init os release
    QSettings osinfo("/etc/os-release", QSettings::IniFormat);
    if (osinfo.contains(QString("PROJECT_CODENAME"))) {
        m_project_codename = osinfo.value(QString("PROJECT_CODENAME")).toString();
    }
}

void SlideGesture::registerWidget(QWidget *widget)
{
    if (!widget) {
        qDebug() << __FILE__ << __FUNCTION__ << " widget is empty";
        return;
    }

    widget->installEventFilter(this);

#ifdef DEBUG_MODE
    qDebug() << __FILE__ << __FUNCTION__ << " register successful";
#endif
}

void SlideGesture::unregisterWidget(QWidget *widget)
{
    if (!widget) {
        qDebug() << __FILE__ << __FUNCTION__ << " widget is empty";
        return;
    }

    // The request is ignored if such an event filter has not been installed
    // It is always safe to remove an event filter,
    // even during event filter activation (i.e. from the eventFilter() function).
    widget->QWidget::removeEventFilter(this);

#ifdef DEBUG_MODE
    qDebug() << __FILE__ << __FUNCTION__ << " unregister successful";
#endif
}

bool SlideGesture::isWidgetUseExtension(QWidget* widget)
{
    if (widget->property(PROPERTY_NOT_USE_GESTURE).toBool())
        return false;
    return true;
}

bool SlideGesture::isWidgetSlideNoScrollBar(QWidget *widget) const
{
    if (widget->property(PROPERTY_SLIDE_NO_SCROLLBAR).toBool())
        return true;
    return false;
}

bool SlideGesture::eventFilter(QObject *watched, QEvent *event)
{
    if(!watched) {
        qDebug() << __FILE__ << __FUNCTION__ << __LINE__ << "watched is empty pointer";
        return false;
    }
    //! \note 过滤掉多余的事件
    //! 一些事件会发生在设置parent之前，这种事件会让widget因在祖先中找不到scrollarea而unregisterWidget
    //! 因此我们过滤掉多余的事件
    //! 注意如果希望在 eventFilter 中处理更多的事件，需要将希望处理的事件加在这里
    switch (event->type()) {
    case QEvent::MouseButtonPress:
    case QEvent::MouseMove:
    case QEvent::MouseButtonRelease:
    case QEvent::Scroll:
    case QEvent::LayoutRequest:
        break;
    default:
        return false;
    }

    /*! \note
     * 在watched的对象树上搜索 QAbstractScrollArea ，
     * 如果找不到，认为这个widget无关滚动，无视他的事件
     * 如果能找到，则在QAbstractScrollArea 上增加滚动器
    */
    QObject *scrollareaWidget = nullptr;
    if(watched->inherits("QAbstractScrollArea")) {
        auto scrollarea = static_cast<QAbstractScrollArea*> (watched);
        //! \note 没有滚动条的位置我们认为不应该进行滚动，也就不注册滚动插件
        if (isScrollAreaWithScrollBar(scrollarea))
            scrollareaWidget = watched;
    }

    if (!scrollareaWidget) {
        auto p = watched;
        while (p) {
            if (QString(p->metaObject()->className()) == "QComboBoxListView") {
                scrollareaWidget = p;
                break;
            }
            if (p->inherits("QAbstractScrollArea")) {
                auto scrollarea = static_cast<QAbstractScrollArea*> (p);
                //! \note 如果横向纵向滚动条都没就继续往上找，避免点击在没有滚动条的内层 scrollarea 导致外层有滚动条的 scrollarea 也无法滚动
                if (isScrollAreaWithScrollBar(scrollarea)) {
                    scrollareaWidget = p;
                    break;
                }
            }
            p = p->parent();
        }
    }

    if (!scrollareaWidget) {
        return false;
    }

    if (!isWidgetUseExtension(static_cast<QWidget*>(scrollareaWidget))) {
        //! \note 由于在事件过滤器里会通过向上遍历对象树方式去搜索QAbstractScrollArea，因此需要以这种方式
        //! 防止没有在children widget setProperty导致插件功能被错误加载
        qDebug() << "Object " << watched->objectName() << "has been set notUseSlideGesture";
        QWidget* wid = static_cast<QWidget*>(watched);
        this->unregisterWidget(wid);
        return false;
    }

    switch (event->type()) {
    case QEvent::Scroll:{
        if (static_cast<QScrollEvent *>(event)->scrollState() == QScrollEvent::ScrollFinished && QScroller::hasScroller(scrollareaWidget)) {
            if (QScroller::scroller(scrollareaWidget)->state() == QScroller::State::Inactive) {
#ifdef DEBUG_MODE
                qDebug() << __FILE__ << __FUNCTION__ << __LINE__;
#endif
                QScroller::scroller(scrollareaWidget)->deleteLater();
            }
        }
        break;
    }

    case QEvent::MouseButtonPress:{
        //! \note 接收事件的 widget 可能是 scrollareaWidget 的 children，而非 scrollareaWidget
        //! 因此坐标要转换为 scrollareaWidget 的坐标进行处理，不然handleInput会导致滚动行为异常
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        QWidget*     wid = static_cast<QWidget*>(scrollareaWidget);
        m_press_pos = wid->mapFromGlobal(mouseEvent->globalPos());

#ifdef DEBUG_MODE
        qDebug() << __FILE__ << __FUNCTION__ << __LINE__;
#endif

        if (mouseEvent->source() == Qt::MouseEventSynthesizedByQt) {
            if (mouseEvent->button() == Qt::LeftButton) {
                if (QScroller::hasScroller(scrollareaWidget)) {
                    QScroller::scroller(scrollareaWidget)->deleteLater();
                }
                m_touch_slide_timer->start(LONG_PRESS_TIME);
            }
        }
        else if (QScroller::hasScroller(scrollareaWidget)) {
            QScroller::scroller(scrollareaWidget)->deleteLater();
        }
        break;
    }

    case QEvent::MouseMove:{
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        if (mouseEvent->source() == Qt::MouseEventSynthesizedByQt) {
            if (QScroller::hasScroller(scrollareaWidget)) {
                return true;
            }

            if (m_touch_slide_timer->isActive()) {
                //! \note 接收事件的 widget 可能是 scrollareaWidget 的 children，而非 scrollareaWidget
                //! 因此坐标要转换为 scrollareaWidget 的坐标进行处理，不然handleInput会导致滚动行为异常
                QWidget* wid = static_cast<QWidget*>(scrollareaWidget);
                QPoint pos = wid->mapFromGlobal(mouseEvent->globalPos());

                if ((m_press_pos - pos).manhattanLength() > 1.5 * QApplication::startDragDistance()) {
                    if (scrollareaWidget->inherits("QAbstractItemView")) {
                        QAbstractItemView* listView = static_cast<QAbstractItemView *>(scrollareaWidget);
                        listView->setVerticalScrollMode(QAbstractItemView::ScrollMode::ScrollPerPixel);
                        listView->setHorizontalScrollMode(QAbstractItemView::ScrollMode::ScrollPerPixel);
                    }
#ifdef DEBUG_MODE
                    qDebug() << __FILE__ << __FUNCTION__ << __LINE__;
#endif
                    QScroller::grabGesture(scrollareaWidget, QScroller::LeftMouseButtonGesture);
                    QScroller *scroller = QScroller::scroller(scrollareaWidget);
                    if (0 == m_project_codename.compare("V10SP1-edu", Qt::CaseInsensitive)) {
                        QScrollerProperties properties = scroller->scrollerProperties();
                        properties.setScrollMetric(QScrollerProperties::HorizontalOvershootPolicy, QScrollerProperties::OvershootAlwaysOff);
                        properties.setScrollMetric(QScrollerProperties::VerticalOvershootPolicy, QScrollerProperties::OvershootAlwaysOff);
                        scroller->setScrollerProperties(properties);
                    }
                    scroller->handleInput(QScroller::InputPress, pos, static_cast<qint64>(mouseEvent->timestamp()));
                    scroller->handleInput(QScroller::InputMove, pos, static_cast<qint64>(mouseEvent->timestamp()));
                }
                return true;
            }
        }
        else if (QScroller::hasScroller(scrollareaWidget)) {
            QScroller::ungrabGesture(scrollareaWidget);
            QScroller::scroller(scrollareaWidget)->deleteLater();
        }
        break;
    }

    case QEvent::LayoutRequest: {
        //! \note 如果当前widget没有qscroller，QScroller::scroller()会创建一个，
        //! 因此如果在 deleteLatter 删除了QScroller之后，在 QScroller::hasScroller 进行判断之前，
        //! 调用了 QScroller::scroller()，则可能会导致 QScroller::hasScroller 的结果不是我们想要的结果
        //! 如果你只希望获取当前 scroller，务必在使用 QScroller::scroller() 前判断当前是否存在scroller
        //! 除非你希望当前没有 scroller 的话创建一个。
        if (!QScroller::hasScroller(scrollareaWidget)) {
            break;
        }
        if (QScroller::scroller(scrollareaWidget)->state() == QScroller::Dragging) {
            qDebug() << __FILE__ << __FUNCTION__ << "Ignore QEvent::LayoutRequest because scorller is draging";
            return true;
        }

        break;
    }

    default:
        break;
    }
    return false;
}

bool SlideGesture::isScrollAreaWithScrollBar(QAbstractScrollArea *scrollarea) const
{
    if (isScrollAreaWithHorizontalScrollBar(scrollarea)
            || isScrollAreaWithVerticalScrollBar(scrollarea)
            || isWidgetSlideNoScrollBar(scrollarea)) {
        return true;
    }
    return false;
}

bool SlideGesture::isScrollAreaWithHorizontalScrollBar(QAbstractScrollArea *scrollarea) const
{
    QScrollBar* horizontalScrollBar = scrollarea->horizontalScrollBar();
    if (horizontalScrollBar != nullptr
            && horizontalScrollBar->isVisible()
            && scrollarea->horizontalScrollBarPolicy() != Qt::ScrollBarAlwaysOff) {
        return true;
    }
    return false;
}

bool SlideGesture::isScrollAreaWithVerticalScrollBar(QAbstractScrollArea *scrollarea) const
{
    QScrollBar* verticalScrollBar = scrollarea->verticalScrollBar();
    if (verticalScrollBar != nullptr
            && verticalScrollBar->isVisible()
            && scrollarea->verticalScrollBarPolicy() != Qt::ScrollBarAlwaysOff) {
        return true;
    }
    return false;
}

} // namespace
